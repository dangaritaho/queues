import pika
import time
import json

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit'))
channel = connection.channel()

channel.queue_declare(queue='conferences')

index = 0

while (True):
    time.sleep(1)
    channel.basic_publish(
        exchange='',
        routing_key='conferences',  # name of the queue
        body=json.dumps({
            "conference_id": index
        })  # information sent through the queue
    )

    print(f" [x] Sent conference {index}")

    index += 1

# channel.basic_publish(exchange='',
#                       routing_key='conferences',
#                       body='Sending conferences!')
# print(" [x] Sent a conference")

connection.close()
